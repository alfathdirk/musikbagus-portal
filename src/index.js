import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';

import stores from './stores';
import AppNavigator from './router/router';
import './utils/notification';

import '../assets/scss/app.scss';
import '../assets/css/global.css';

document.body.removeAttribute('unresolved');

const App = () => (
  <Provider {...stores}>
    <AppNavigator />
  </Provider>
);

ReactDOM.render(<HashRouter><App /></HashRouter>, document.getElementById('root'));

export default App;
