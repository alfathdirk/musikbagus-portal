import { observable, action } from 'mobx';
import themes from '../themes';

class Theme {
  @observable currentTheme = themes.default.default();

  @action changeTheme = (currentTheme) => {
    try {
      this.currentTheme = themes[currentTheme].default();
    } catch (error) {
      console.error(`${currentTheme}_theme.js not found on folder Themes`);
    }
  }
}

export default Theme;
