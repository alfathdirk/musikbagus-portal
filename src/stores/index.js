import React from 'react';
import { create } from 'mobx-persist';
// import localForage from 'localforage';

import AccountStore from './Account';
// import ThemeStore from './Theme';

const hydrate = create({ storage: window.localStorage });
// const hydrate = create({ storage: localForage });

const stores = {
  accountStore: new AccountStore(),
  // themeStore: new ThemeStore(),
};

hydrate('accountStore', stores.accountStore);
// hydrate('themeStore', stores.themeStore);

const storesContext = React.createContext(stores);

export default storesContext;
