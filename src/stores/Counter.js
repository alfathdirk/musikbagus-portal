import { observable, action } from 'mobx';

export default class CounterStore {
  @observable count = 0;

  @action onPlus () {
    this.count += 1;
  }

  @action onMinus () {
    this.count -= 1;
  }

  @action anu () {
    let count = this.count * 5;
    this.count = count;
  }
}
