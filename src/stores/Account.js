import { observable, action } from 'mobx';
import { persist } from 'mobx-persist';

class SchemeAccount {
  @persist @observable username = '';
  @persist @observable token = '';
  @persist @observable email = '';
}

class Account {
  @persist('object', SchemeAccount) @observable user = new SchemeAccount();

  @persist @observable authorized = false;
  @persist @observable showOnboarding = true;
  @observable refGrid = '';

  @action login = (username, password) => {
    this.authorized = true;
    return { username, password };
  }

  @action setHasViewOnboarding = () => {
    this.showOnboarding = false;
  }

  @action setOnboardingView = (isShow) => {
    this.showOnboarding = isShow;
  }

  @action setRef = (f) => {
    this.refGrid = f;
  }

  @action logout = () => {
    return new Promise((resolve, reject) => {
      this.authorized = false;
      this.user = new SchemeAccount();
      resolve();
    });
  }
}

export default Account;
