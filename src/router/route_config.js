import { lazy } from 'react';

const Login = lazy(() => import('../features/login/login_feature'));
const Home = lazy(() => import('../features/home/home_feature'));

const Routers = {
  login: {
    component: Login,
    isNeedAuth: false,
    showMenuTab: false,
    isDashboardPage: false,
  },
  home: {
    component: Home,
    isNeedAuth: false,
    showMenuTab: true,
    isDashboardPage: true,
  },
};

export default Routers;
