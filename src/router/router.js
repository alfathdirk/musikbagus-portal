import React, { Suspense, lazy, useState, useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { bounceInUp } from 'react-animations';

import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { createBrowserHistory } from 'history';
import { useStores } from '../hooks/hook';
import { observe } from 'mobx';
import { observer } from 'mobx-react';
import Radium, { StyleRoot } from 'radium';

import routeConfig from './route_config';
// import debounce from 'lodash.debounce';

const browserHistory = createBrowserHistory();
const routeStore = new RouterStore();
const histories = syncHistoryWithStore(browserHistory, routeStore);

const Home = lazy(() => import('../features/home/home_feature'));

const CustomRoute = ({ component: Component, isNeedAuth, isLogin, isDashboardPage, setIsDashboardPage, setHeaderBar, ...rest }) => {
  useEffect(() => {
    setIsDashboardPage(isDashboardPage);
    return () => {
      setIsDashboardPage(true);
      setHeaderBar({});
    };
  }, []);
  return (
    <Route
      {...rest}
      render={(props) => (
        (isNeedAuth !== true || isLogin === true)
          ? <Component {...props} setHeaderBar={setHeaderBar} params={rest.location.pathname.split('/').slice(1)} />
          : <Redirect to={`/login?continue=${rest.path}`} />
      )}
    />
  );
};

const NotFound = () => (
  <p>Not Found euy!</p>
);

const AppNavigator = observer((props) => {
  const styles = {
    fadeIn: {
      animation: 'x 0.5s',
      animationName: Radium.keyframes(bounceInUp, 'bounceInUp'),
      backgroundColor: '#fff',
    },
  };

  const { accountStore } = useStores();
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isDashboardPage, setIsDashboardPage] = useState(true);
  const [pathName, changePathName] = useState(browserHistory.location.pathname);
  const [animation, changeAnimation] = useState(styles);
  const [headerBar, setHeaderBar] = useState({});

  useEffect(() => {
    observe(routeStore, (change) => {
      changePathName(browserHistory.location.pathname);
    });
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    changeAnimation(styles);
    setLoggedIn(accountStore.authorized);
  }, [pathName]);

  useEffect(() => {
    setTimeout(() => {
      changeAnimation('');
    }, 300);
  }, [animation]);

  return (
    <div>
      <StyleRoot>
        <div className="bodyAnimation" style={animation.fadeIn}>
          <Router history={histories}>
            <Suspense fallback={<p>loading...</p>}>
              <Switch>
                <Route exact path='/' component={Home} />
                {Object.keys(routeConfig).map((routeName, k) => {
                  let route = routeConfig[routeName];
                  return (
                    <CustomRoute
                      key={k}
                      path={`/${routeName}`}
                      component={route.component}
                      isNeedAuth={route.isNeedAuth}
                      isLogin={isLoggedIn}
                      isDashboardPage={route.showMenuTab}
                      setIsDashboardPage={setIsDashboardPage}
                      setHeaderBar={setHeaderBar}
                    />
                  );
                })}
                <Route path="*" component={NotFound} />
              </Switch>
            </Suspense>
          </Router>
        </div>
      </StyleRoot>
    </div>
  );
});

export default AppNavigator;
