import { createMuiTheme } from '@material-ui/core/styles';

const defaultThemes = createMuiTheme.bind(this, {
  '@global': {
    '*::-webkit-scrollbar': {
      width: '0',
    },
  },
  palette: {
    text: {
      primary: '#000000',
      secondary: '#fff',
    },
    primary: {
      main: '#f58523',
      light: '#757ce8',
      dark: '#002884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#fff',
    },
  },
  overrides: {
    MuiCardContent: {
      root: {
        padding: 12,
      },
    },
    MuiCssBaseline: {
      '@global': {
        body: {
          background: '#fff',
        },
      },
    },
    MuiPaper: {
      elevation4: {
        boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.15)',
      },
    },
    MuiTypography: {
      body1: {
        fontSize: 16,
        fontWeight: 600,
        lineHeight: 1.31,
      },
      body2: {
        fontSize: 14,
      },
      subtitle1: {
        fontSize: 12,
      },
      subtitle2: {
        fontSize: 10,
      },
      h6: {
        fontSize: 20,
        fontWeight: 600,
      },
      h1: {
        fontSize: 32,
        fontWeight: 600,
      },
    },
  },
  status: {
    danger: 'orange',
  },
  typography: {
    fontFamily: [
      '"proxima-nova"',
    ].join(','),
  },
});

export default defaultThemes;
