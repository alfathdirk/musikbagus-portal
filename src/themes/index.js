let req = require.context('../themes', true, /\.js$/);
let files = {};
req.keys().forEach((filename) => {
  let name = filename.split('_')[0].slice(2);
  if (name !== 'index.js') {
    Object.assign(files, { [name]: req(filename) });
  }
});

export default files;
