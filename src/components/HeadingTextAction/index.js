import React from 'react';
import { Typography } from '@material-ui/core';

export default function HeadingTextAction (props) {
  const { rightAction, leftText, rightText, rightColor } = props;
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between', margin: '6px 10px 0px 12px' }}>
      <Typography gutterBottom variant="h6" component="h2">
        {leftText || 'Title'}
      </Typography>
      <Typography style={{ marginTop: 5, fontWeight: 'normal' }} align="justify" onClick={rightAction} color="primary" variant="body1">
        {rightText}
      </Typography>
    </div>
  );
}
