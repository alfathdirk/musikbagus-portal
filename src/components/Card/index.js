import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

const useStyles = makeStyles({
  root: {
    maxWidth: 350,
    width: (props) => props.width,
    minWidth: 130,
    height: (props) => props.height,
    minHeight: 130,
    margin: 8,
    borderRadius: 12,
    padding: 0,
    display: 'inline-block',
    // WebkitBoxShadow: '0px 0px 5px 1px #cecccc',
    boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.15)',
  },
});

export default function MediaCard (props) {
  const classes = useStyles(props);
  let { data, component, headerCard } = props;

  if (!data) {
    let components = (v) => {
      return (
        <div style={{ textAlign: 'center' }}>
          <img src="assets/images/profile/200x200manny.png" style={{ width: 70, height: 70 }} />
          <div>{v}</div>
        </div>
      );
    };
    data = ['Hello', 'World', 'Meteor'];
    component = components;
  }

  return (
    <div style={{ overflowX: 'scroll' }}>
      <div style={{ width: '100%', overflow: 'auto', whiteSpace: 'nowrap', paddingLeft: 10, paddingRight: 10 }}>
        {data.map((v, k) => (
          <Card key={k} className={classes.root}>
            <CardActionArea>
              {headerCard && headerCard(v)}
              <CardContent>
                {component(v)}
              </CardContent>
            </CardActionArea>
          </Card>
        ))}
      </div>
    </div>
  );
}
