import React from 'react';
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import { Restaurant, MenuBookOutlined, LocationOffOutlined, Home } from '@material-ui/icons';
import { useStyles } from './styles';
import { useStores } from '../../hooks/hook';

export default function BottomBar (props) {
  let { history, onTap } = props;
  const classes = useStyles();
  let { accountStore } = useStores();
  let navigate = (route) => () => {
    history.push(route);
  };

  let deleteOnboarding = () => {
    accountStore.setOnboardingView(true);
  };

  return (
    <BottomNavigation
      id="bottom-nav"
      style={{ boxShadow: '0 -2px 24px 0 rgba(0, 0, 0, 0.08)' }}
      value={'val'}
      onChange={(event, newValue) => {}}
      showLabels
    >
      <BottomNavigationAction label="Recents" icon={<Home />} onClick={navigate('/home')} className={classes.root} />
      <BottomNavigationAction label="Favorites" icon={<MenuBookOutlined />} onClick={navigate('/products')} className={classes.root} />
      <BottomNavigationAction label="Nearby" icon={<LocationOffOutlined />} className={classes.root} />
      <BottomNavigationAction label="Onboarding" icon={<Restaurant />} onClick={deleteOnboarding} className={classes.root} />
    </BottomNavigation>
  );
}
