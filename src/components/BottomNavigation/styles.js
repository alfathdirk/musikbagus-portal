import { makeStyles, lighten } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  /* Styles applied to the root element. */
  root: {
    color: '#858585',
    '&$selected': {
      color: theme.palette.primary.main,
    },
  },
  /* Styles applied to the root element if selected. */
  selected: {},
}));
