import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  roots: {
    width: window.innerWidth - 30,
    height: 120,
    margin: '0 auto',
    borderRadius: 12,
    boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.15)',
    position: 'relative',
    top: 10,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 12,
  },
  progress: {
    borderRadius: 8,
    height: 6
  },
}));
