import React from 'react';
import { Typography, Grid, LinearProgress } from '@material-ui/core';
import { useStyles } from './styles';

export default function BottomBar (props) {
  const classes = useStyles();

  return (
    <div className={classes.roots}>
      <Grid item xs={12}>
        <Grid container>
          <Grid item>
            <img src="./assets/images/logo.png" className={classes.image} />
          </Grid>
          <Grid item style={{ alignSelf: 'center', flex: 1, marginLeft: 10, marginRight: 10 }}>
            <Typography style={{ marginTop: 5 }} color="textPrimary" variant="body1">
              Penggalangan Dana Anak Kolong Jembatan
            </Typography>
            <Typography style={{ marginBottom: 5 }} color="textPrimary" variant="subtitle1" component="p">
              Nama Organisasi PT International
            </Typography>
            <LinearProgress
              className={classes.progress}
              variant="determinate"
              color="primary"
              value={78}
            />
            <div style={{ display: 'flex', marginTop: 5 }}>
              <Typography variant="body2" color="textPrimary" component="p" style={{ whiteSpace: 'pre-line', marginRight: 15 }}>
                Terkumpul
              </Typography>
              <Typography variant="body2" color="textPrimary" style={{ whiteSpace: 'pre-line', fontWeight: 600 }}>
                RP. 10.000.000
              </Typography>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
