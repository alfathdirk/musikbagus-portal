import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
// align-items: center;
// display: flex;
const useStyles = makeStyles({
  root: {
    marginBottom: 20,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  boxBorder: {
    // border: '1px solid #cecccc',
    width: (props) => window.innerWidth / (props.row || 3) - 20,
    height: (props) => window.innerWidth / (props.row || 3) - 20,
    padding: 7,
    margin: 8,
    textAlign: 'center',
    borderRadius: 12,
    // WebkitBoxShadow: '0px 0px 5px 1px #cecccc',
    boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.15)',
    justifyContent: 'center',
    // alignItems: 'center',
    display: 'flex',
  },
});

export default function BoxGrid (props) {
  const classes = useStyles(props);
  let { data, component, onClickItem } = props;

  if (!data) {
    let components = (v) => {
      return (
        <div>
          <img src="assets/images/profile/200x200manny.png" style={{ width: 70, height: 70 }} />
          <div>{v}</div>
        </div>
      );
    };
    data = ['Hello', 'World', 'Meteor'];
    component = components;
  }

  return (
    <Grid item xs={12} className={classes.root}>
      <Grid container spacing={0} className={classes.container}>
        {data.map((v, k) => (
          <Grid key={k} item className={classes.boxBorder} onClick={() => onClickItem(v)}>
            {component(v)}
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
}
