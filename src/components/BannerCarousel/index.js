import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Slider from 'react-slick';
import { BASE_URL_BANNER } from '../../utils/Constants';

const useStyles = makeStyles({
  root: {
    borderRadius: 8,
    height: 190,
    width: window.innerWidth - 30,
    objectFit: 'cover',
    margin: '0 auto',
    // border: '1px solid white',
  },
  sliders: {
    color: 'white',
    // position: 'absolute',
    // top: '34%',
    // left: '50%',
    // transform: 'translate(-50%, -50%)',
    // zIndex: 2,
    // width: '100%',
    textAlign: 'center',
  },
  backgroundSlider: {
    backgroundColor: 'red',
  },
});
export default function SimpleSlider (props) {
  const classes = useStyles(props);
  const { data } = props;
  return (
    <>
      <div style={{ position: 'relative', overflow: 'hidden' }}>
        <div style={{ textAlign: 'center', borderBottomLeftRadius: 90, borderBottomRightRadius: 90, height: 229, background: '#f58522', position: 'absolute', width: window.innerWidth + 50, left: -25 }} />
        {/* <div style={{ textAlign: 'center', borderBottomLeftRadius: 90, borderBottomRightRadius: 90, height: 229, backgroundImage: `url(${dataImage[backgroundIndex].image})`, WebkitFilter: 'blur(3px)', position: 'absolute', width: window.innerWidth + 50, left: -25 }} /> */}
        <div className={classes.sliders}>
          <div style={{ position: 'absolute', margin: 10 }}>
            <Typography gutterBottom variant="h1" component="h2" style={{ textAlign: 'left', margin: 10 }}>
              Hi, Evan
            </Typography>
            <Typography gutterBottom variant="h6" component="h2" style={{ textAlign: 'left', margin: 10 }}>
              Let’s start Giving
            </Typography>
          </div>
          <div style={{ paddingTop: 120 }}>
            <Slider infinite>
              {data.map((v, k) => (
                <div key={k} style={{ textAlign: 'center', margin: '0 auto', width: window.innerWidth - 25 }}>
                  <img src={BASE_URL_BANNER + v.banner} className={classes.root} />
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </>
  );
}
