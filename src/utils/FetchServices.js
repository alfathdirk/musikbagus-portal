import axios from 'axios';

class FetchData {
  constructor (url, body = {}) {
    this.head = {};
    this.url = url;
    this.head = {
      method: 'GET',
      url,
      baseURL: 'https://dev.svcgateway.meteor.asia',
      headers: {
        'Content-Type': 'application/json',
        'x-app-key': 'U2FsdGVkX19YWTfvogMG0qQp5pdOn5CMhFBr1CHXvyRPnhVVcvM0Nr/S44qwWlOS',
      },
    };

    this.body = body;
  }

  async exec () {
    let accountStore = JSON.parse(window?.localStorage?.accountStore);
    if (accountStore?.user?.token) {
      if (this.url !== '/banner') {
        Object.assign(this.head.headers, { Authorization: 'Bearer ' + accountStore?.user?.token });
      }
    }
    if (this.body) {
      let data = this.head.method === 'GET' ? 'params' : 'data';
      Object.assign(this.head, {
        [data]: this.body,
      });
    }
    try {
      let response = await axios(this.head);
      return {
        data: response.data.value,
        status: response.data.status,
      };
    } catch (error) {
      let err = new Error(error.message);
      throw err;
    }
  }

  get () {
    this.head.method = 'GET';
    return this.exec();
  }

  post () {
    this.head.method = 'POST';
    return this.exec();
  }

  put () {
    this.head.method = 'PUT';
    return this.exec();
  }

  delete () {
    this.head.method = 'DELETE';
    return this.exec();
  }
}

const fetchData = (url, body = {}) => {
  let fetch = new FetchData(url, body);
  return fetch;
};

export default fetchData;
