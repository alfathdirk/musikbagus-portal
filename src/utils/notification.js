const firebase = require('firebase/app');

require('firebase/auth');
require('firebase/messaging');

const isSubscribed = false;
const applicationServerKey = urlB64ToUint8Array('BDT1wRwdIYjQJ5mEDFBW5Nbxn_5bAznfqP2pnqZeUyTgaOBlbEuB5xLBc3b-5rDl_cMIrpZkn2_ncQWmRk13YfM');
let firebaseConfig = {
  apiKey: 'AIzaSyAWk3lUgHyhd3K5SRnl7W3464DnZRnqQy0',
  authDomain: 'react-boilerplate-290b9.firebaseapp.com',
  databaseURL: 'https://react-boilerplate-290b9.firebaseio.com',
  projectId: 'react-boilerplate-290b9',
  storageBucket: 'react-boilerplate-290b9.appspot.com',
  messagingSenderId: '262606725563',
  appId: '1:262606725563:web:9c268ad13e94f8e185649e',
  measurementId: 'G-RLJ4ETWTV1',
};

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('./firebase-messaging-sw.js')
      .then(registration => {
        console.info('SW registered: ', registration);
        let swRegistration = registration;
        swRegistration.pushManager.getSubscription().then((subscription) => {
          if (!isSubscribed) {
            swRegistration.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey,
            }).then((subscription) => {
              console.info('User is subscribed.');
            });
          }
        });
      }).catch(registrationError => {
        console.info('SW registration failed: ', registrationError);
      });
  });
  firebase.initializeApp(firebaseConfig);

  const messaging = firebase.messaging();
  messaging.usePublicVapidKey('BDT1wRwdIYjQJ5mEDFBW5Nbxn_5bAznfqP2pnqZeUyTgaOBlbEuB5xLBc3b-5rDl_cMIrpZkn2_ncQWmRk13YfM');

  messaging.getToken().then((currentToken) => {
    if (currentToken) {
      console.info('save kee db', currentToken);
    } else {
      console.info('No Instance ID token available. Request permission to generate one.');
    }
  }).catch((err) => {
    console.info('Error retrieving Instance ID token. ', err);
  });
}

function urlB64ToUint8Array (base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}
