/* eslint-disable */
workbox.core.skipWaiting();
workbox.core.clientsClaim();

const APP_CACHE_NAME = 'portal-v2';
const urlsCache = ['/', '/assets/js/global.js', '/assets/css/global.css', '/assets/fonts/ProximaNova-Regular.otf'];

self.addEventListener('install', event => {
  event.waitUntil(caches.open(APP_CACHE_NAME).then(cache => cache.addAll(urlsCache)));
});

self.addEventListener('activate', (event) => {
  event.waitUntil(caches.keys().then(
    (cachesNames) => {
      return Promise.all(cachesNames.filter((cacheName) => {
        console.log('>>>>',cacheName);
      }).map((cacheName) => caches.delete(cacheName)))
    }))
});

self.addEventListener('notificationclick', function(event) {
  self.registration.getNotifications().then(function(notifications) {
    notifications.forEach(function(notification) {
      notification.close();
    });
  });
  let url = 'http://localhost:8443/';
    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                console.log('>>',client);
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});

self.addEventListener('push', async (event) => {
  let dataNotif = await event.data.json();
  let data = JSON.parse(dataNotif.data.custom_notification);
  const notificationTitle = data.title;
  const options = {
    body: data.body,
    icon: data.icon,
    badge: 'images/badge.png',
  };

  event.waitUntil(self.registration.showNotification(data, options));
});

self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
