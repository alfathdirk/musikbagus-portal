# Mobile Builder Application PWA

This repo for Mobile Builder Application Flutter

# Installation
```bash
npm i
```

# Create new module builder
```
npm run generate -- [module name] -a --store --menu

ex: 
// single module
npm run generate -- article -a --store --menu

// multiple module
npm run generate -- article music movies -a --store --menu

// -a with auth
// -store with store
// -menu show menu on tab bar
```
