const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const { InjectManifest, GenerateSW } = require('workbox-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    index: ['@babel/polyfill', './index.js'],
  },
  output: {
    path: path.join(__dirname, 'www'),
    filename: 'lib/[name].js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          publicPath: 'assets',
        },
      },
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [
              ['@babel/plugin-proposal-decorators', { legacy: true }],
              ['@babel/plugin-proposal-class-properties'],
            ],
          },
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: false },
          },
        ],
      },
      {
        test: /\.s?css$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
        // options: {
        //   publicPath: 'assets',
        // },
      },
      {
        test: /\.(svg|png|jpe?g|gif)(\?.*)?$/i,
        use: {
          loader: 'url-loader',
          options: {
            limit: 50,
            name: 'images/[name].[ext]',
          },
        },
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        use: {
          loader: 'file-loader',
          options: {
            limit: 50,
            name: 'fonts/[name].[ext]',
          },
        },
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    disableHostCheck: true,
    // liveReload: false,
    contentBase: path.join(__dirname, 'www'),
    // https: true,
    port: 8443,
    host: '0.0.0.0',
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './index.html' }),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, './assets'), to: './assets' },
    ]),
    new WebpackPwaManifest({
      name: 'MyAppPWA',
      short_name: 'MyPWA',
      description: 'My awesome Progressive Web App!',
      background_color: '#ffffff',
      crossorigin: 'use-credentials', // can be null, use-credentials or anonymous
      icons: [
        {
          src: path.resolve('./assets/icons/icon.png'),
          sizes: [96, 128, 192, 256, 384, 512], // multiple sizes
        },
        {
          src: path.resolve('./assets/icons/large-icon.png'),
          size: '267x268', // you can also use the specifications pattern
        },
      ],
    }),
    new InjectManifest({
      swSrc: path.resolve(__dirname, './src/utils/firebase-messaging-sw.js'),
    }),
    // new GenerateSW({
    //   swDest: './utils/firebase-messaging-sw.js',
    //   // importWorkboxFrom: 'local',
    //   skipWaiting: true,
    //   clientsClaim: true,
    // //   // globPatterns: [
    // //   //   'fonts/**/*.{eot,svg,ttf,woff,woff2}',
    // //   // ],
    // }),
  ],
};
